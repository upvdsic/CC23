
package component

#Artifact: {
  ref: name:  "codeserver"
  description: {
    srv: {
      server: web: port: 8080
      client: kafkacli: _
    }

    config: {}
    
    size: {
      bandwidth: {
        size: 1000
        unit: "M"
      }
    }

    code: mainserver: {
      name: "mainserver"
      image: {
        tag: "codercom/code-server"
      }

      user: {
        userid : 0  // set the right value for this
        groupid: 0
      }

      cmd: ["--auth", "none", "--bind-addr", "0.0.0.0:\(srv.server.web.port)"]

      size: {
        memory: {size: 1, unit: "G"}
        cpu:    {size: 2000, unit: "m"}
        mincpu: 2000
      }
    }

  }
}
