
package service

import (
  k  "kumori.systems/kumori:kumori"
  i  "kumori.systems/builtins/inbound:service"
  p  ".../psql:component"
  kc ".../keycloak:component"
)

#Artifact: {
  ref: name:  ""
  description: {
    srv: {
    }

    config: {
      resource: {
        kc_password: k.#Secret
        store      : k.#Volume
        domain     : k.#Domain
        cert       : k.#Certificate
      }
    }

		let _cfgr = description.config.resource

    let const = {
      dbName        : "keycloak"
      dbUsername    : "keycloak"
      dbPassword    : "keycloak"
    }

    role: {
      kcinbound: {
        artifact: i.#Artifact

        config: {
          parameter: {
            type: "https"
            websocket: true
          }
          resource: {
            servercert  : _cfgr.cert
            serverdomain: _cfgr.domain
          }

          resilience: description.config.resilience
        }
        
      }

      keycloak: {
        artifact: kc.#Artifact 
        config: {
          parameter: {
            dbType        : "postgres"
            dbName        : const.dbName
            dbUsername    : const.dbUsername
            dbPassword    : const.dbPassword
            masterUsername: "admin"
          }
          resource: {
            masterPassword: _cfgr.kc_password
          }
        }
      }

      psql: {
        artifact: p.#Artifact
        config: {
          parameter: {
            dbname  : const.dbName
            username: const.dbUsername
            password: const.dbPassword
          }
          resource: store: _cfgr.store
        }
      }
    }

    connect: {
      ckeycloak: {
        as: "lb"
        to: keycloak: restapi: _

  			from: kcinbound: "inbound"
      }     

      cpsql: {
          as: "lb"
          to: psql: main: _ 

          from: keycloak: "dbcli"
      }
    }
  }
}
