package kmodule

{
	domain: "a.b"
	module: "d"
	version: [
		0,
		0,
		1,
	]
	cue: "v0.4.3"
	spec: [
		1,
		0,
	]
	dependencies: {
		"kumori.systems/kumori": {
			query:  "latest"
			target: "kumori.systems/kumori/@1.1.0"
		}
		"kumori.systems/builtins/inbound": {
			query:  "latest"
			target: "kumori.systems/builtins/inbound/@1.2.0"
		}
	}
	sums: {}
}
