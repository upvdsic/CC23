
package service

import (
  k  "kumori.systems/kumori:kumori"
  i  "kumori.systems/builtins/inbound:service"
  c  ".../codeserver:component"
  kf ".../kafka:component"
  z  ".../zookeeper:component"
)

#Artifact: {
  ref: name:  ""
  description: {
    srv: server: discover: {port: 9092, protocol: "tcp"}

    config: {
      resource: {
        kafkastore: k.#Volume
        zoostore  : k.#Volume
        domain    : k.#Domain
        cert      : k.#Certificate
      }
    }

		let _cfgr = description.config.resource

    role: {
      csinbound: {
        artifact: i.#Artifact 
        config: {
          parameter: {
            type: "https"
            websocket: true
          }
          resource: {
            servercert  : _cfgr.cert
            serverdomain: _cfgr.domain
          }

          resilience: description.config.resilience
        }
      }

      codeserver: {
        artifact: c.#Artifact
      }

      kafka: {
        artifact: kf.#Artifact 
        config: {
          resource: {
            kafkastore: _cfgr.kafkastore
          }
        }
      }

      zookeeper: {
        artifact: z.#Artifact
        config: {
          resource: zoostore: _cfgr.zoostore
        }
      }
    }

    connect: {
      ccode: {
        as: "lb"
        to: codeserver: web: _ 

        from: csinbound: "inbound"
      }

      czoo: {
        as: "lb"
        to: zookeeper: main: _

  			from: kafka: "zookeepercli"
      }     

      ckafka: {
          as: "lb"
          to: kafka: advertised: _ 

          from: self: "discover"
          from: codeserver: "kafkacli"
      }
    }
  }
}
