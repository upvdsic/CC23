package deployment

import (
    s ".../kafka:service"
    k "kumori.systems/kumori:kumori"
)

#Deployment: k.#Deployment & {
    artifact: s.#Artifact
    name: "josep_kf"
    config: resource: {
        kafkastore: volume: {size: 20, unit: "G"}
        zoostore  : volume: {size: 2, unit: "G"}
        domain    : domain: "codekafka"
        cert      : certificate: "cluster.core/wildcard-vera-kumori-cloud"
    }

    config: scale: detail: {
        codeserver: hsize: 1
        kafka     : hsize: 1
        zookeeper : hsize: 1
    }
}