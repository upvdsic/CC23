package deployment

import (
    s ".../keycloak:service"
    k "kumori.systems/kumori:kumori"
)

#Deployment: k.#Deployment & {
    artifact: s.#Artifact
    name: "josep_kc"
    config: resource: {
        kc_password: secret     : "blob"
        cert       : certificate: "cluster.core/wildcard-vera-kumori-cloud"
        domain     : domain     : "josep_kc"
        store      : volume     : {size: 2, unit: "G"}
    }

    config: scale: detail: {
        psql    : hsize: 1
        keycloak: hsize: 1
    }
}