
package component

import (
  k "kumori.systems/kumori:kumori"
)

#Artifact: {
  ref: name: "zookeeper"
  description: {
    srv: {
      server: main: port: 2181
    }
    config: {
      resource: zoostore: k.#Volume
    }
    
    size: {
      bandwidth: {
        size: 100
        unit: "M"
      }
    }

    code: mainserver: {
      name: "mainserver"
      image: {
        tag: "bitnami/zookeeper"
      }

      user: {
        userid : 0  // set the right value for this
        groupid: 0
      }

      mapping: {
        // Map a file or volume
        filesystem: {
          "/bitnami": volume: "zoostore"
        }

        env: {
          ZOO_TICK_TIME        : value: "2000"
          ZOO_PORT_NUMBER      : value: "\(description.srv.server.main.port)"
          ALLOW_ANONYMOUS_LOGIN: value: "yes"
        }
      }

      size: {
        memory: {size: 1, unit: "G"}
        cpu:    {size: 2000, unit: "m"}
        mincpu: 500
      }
    }
  }
}
